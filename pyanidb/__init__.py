import socket, time, sys
import status_codes as codes

protover = 3
client = 'pyanimgr'
clientver = 1

red = lambda x: '\x1b[1;31m' + x + '\x1b[0m'
green = lambda x: '\x1b[1;32m' + x + '\x1b[0m'
yellow = lambda x: '\x1b[1;33m' + x + '\x1b[0m'
blue = lambda x: '\x1b[1;34m' + x + '\x1b[0m'
clear = lambda x: x + "\033[K"


states = {
    'unknown': 0,
    'hdd': 1,
    'cd': 2,
    'deleted': 3,
    'shared': 4,
    'release': 5
}

fcodes = (
	'',         'aid',      'eid',      'gid',      'lid',     '',     'depr',     'state',
	'size',     'ed2k',     'md5',      'sha1',     'crc32',   '',     'vcdepth',  '',
	'quality',  'source',   'acodec',   'abitrate', 'vcodec', 'vbitrate', 'vres', 'filetype',
    'dublang',  'sublang',  'length',   'description', 'airdate', '', '', 'anifilename',
    '',         '',         '',         '',             '',         '',     '',     '')

acodes = (
    'eptotal',  'eplast',   'year',     'type',     '', '', '', '',
    'romaji',   'kanji',    'english',  'other',    '', '', '', '',
    'epno',     'epname',   'epromaji', 'epkanji',  '', '', '', '',
    'gname',    'gtag',     '',         '',         '', '', '', 'updated'
)

def_fcode = (
    'aid', 'eid', 'gid', 'lid', 'state',
    'ed2k', 'crc32', 'vcdepth',
    'quality', 'source', 'acodec', 'abitrate', 'vcodec', 'vbitrate', 'vres', 'filetype',
    'dublang', 'sublang', 'length', 'airdate', 'anifilename'
)

def_acode = (
    'eptotal', 'eplast', 'year', 'type',
    'romaji', 'kanji', 'english',
    'epno', 'epname', 'epromaji', 'epkanji',
    'gname', 'gtag', 'updated'
)


def get_mask(all_codes, selected):
    mask = ""
    for c in all_codes:
        if c in selected:
            mask += '1'
        else:
            mask += '0'

    return '%0*X' % ((len(mask) + 3) // 4, int(mask, 2))


class AniDBError(Exception):
    pass


class AniDBTimeout(AniDBError):
    pass


class AniDBLoginError(AniDBError):
    pass


class AniDBUserError(AniDBLoginError):
    pass


class AniDBReplyError(AniDBError):
    pass


class AniDBUnknownFile(AniDBError):
    pass


class AniDBNotInMylist(AniDBError):
    pass


class AniDBUnknownAnime(AniDBError):
    pass


class AniDBUnknownDescription(AniDBError):
    pass


class AniDB:
    def __init__(self, username, password, localport=1234, server=('api.anidb.info', 9000), verbose=False):
        self.localport = localport
        self.sock = None
        self.username = username
        self.password = password
        self.server = server
        self.session = ''
        self.lasttime = 0
        self.verbose = verbose

    def __del__(self):
        if self.sock:
            self.logout()
            self.sock.close()

    def log(self, *args):
        if self.verbose:
            for arg in args:
                print(arg),

            print(clear(''))

    def newver_msg(self):
        self.log('New version available.')

    def retry_msg(self):
        self.log('Connection timed out, retrying.')

    def execute(self, cmd, args=None, retry=False):
        if not self.sock:
            try:
                self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                self.sock.bind(('0.0.0.0', self.localport))
                self.sock.settimeout(10)
                self.auth()
                self.log('{0} {1}'.format(blue('Logged in as user:'), self.username))
            except AniDBUserError:
                print(red('Invalid username/password.'))
                sys.exit(1)
            except AniDBTimeout:
                self.log('Connection timed out.')
                sys.exit(1)
            except AniDBError as e:
                print('{0} {1}'.format(red('Fatal error:'), e))
                sys.exit(1)
            except socket.error as e:
                self.log("Exiting: %s %s" % (e, clear('')))
                sys.exit(1)

        if not args:
            args = {}

        if not 's' in args and self.session:
            args['s'] = self.session

        args['tag'] = str(time.time())[-8:]

        while 1:
            data = '{0} {1}\n'.format(cmd, '&'.join(['{0}={1}'.format(*a) for a in args.items()]))
            t = time.time()
            if t < self.lasttime + 4:
                time.sleep(self.lasttime + 4 - t)

            self.lasttime = time.time()
            self.sock.sendto(data.encode(), 0, self.server)
            try:
                while 1:
                    data = self.sock.recv(8192).decode().split('\n')
                    tag, data[0] = data[0].split(' ', 1)
                    if tag == args['tag']:
                        break

            except socket.timeout:
                if retry:
                    self.retry_msg()
                else:
                    raise AniDBTimeout()
            else:
                break

        code, text = data[0].split(' ', 1)
        data = [line.split('|') for line in data[1:-1]]
        code = int(code)
        return code, text, data

    def ping(self):
        t = time.time()
        try:
            return self.execute('PING')[0] == codes.PONG and time.time() - t or None
        except AniDBTimeout:
            return None

    def auth(self):
        code, text, data = self.execute('AUTH', {'user': self.username, 'pass': self.password, 'protover': protover,
                                                 'client': client, 'clientver': clientver})
        if code in (codes.LOGIN_ACCEPTED, codes.LOGIN_ACCEPTED_NEW_VERSION):
            self.session = text.split(' ', 1)[0]
            if code == codes.LOGIN_ACCEPTED_NEW_VERSION:
                self.newver_msg()
        elif code == codes.LOGIN_FAILED:
            raise AniDBUserError()
        else:
            raise AniDBReplyError(code, text)

    def logout(self):
        if self.session:
            try:
                self.log("Logging out from anidb")
                self.execute('LOGOUT')
                self.session = ''
            except AniDBError:
                pass

    def get_file(self, fid, fields=def_fcode+def_acode, retry=False):
        try:
            size, ed2k = fid
            args = {'size': size, 'ed2k': ed2k}
        except TypeError:
            args = {'fid': fid}
            ed2k = fid

        info_codes = [f for f in (fcodes+acodes) if f in fields]

        args.update({
            'fmask': get_mask(fcodes, fields),
            'amask': get_mask(acodes, fields),
        })

        while 1:
            code, text, data = self.execute('FILE', args, retry)
            if code == codes.FILE:
                filedata = dict([(name, data[0].pop(0)) for name in ['fid'] + info_codes])
                return filedata
            elif code == codes.NO_SUCH_FILE:
                raise AniDBUnknownFile()
            elif code in (codes.LOGIN_FIRST, codes.INVALID_SESSION):
                self.auth()
            else:
                if data:
                    text += ": " + data

                raise AniDBReplyError(code, text)

    def add_file(self, fid, state=None, viewed=False, source=None, storage=None, other=None, edit=False, retry=False):
        try:
            size, ed2k = fid
            args = {'size': size, 'ed2k': ed2k}
        except TypeError:
            args = {'fid': fid}
        if not edit and state == None:
            state = 'hdd'
        if state != None:
            args['state'] = states[state]
        if viewed != None:
            args['viewed'] = int(bool(viewed))
        if source != None:
            args['source'] = source
        if storage != None:
            args['storage'] = storage
        if other != None:
            args['other'] = other
        if edit:
            args['edit'] = 1

        while 1:
            code, text, data = self.execute('MYLISTADD', args, retry)
            if code in (codes.MYLIST_ENTRY_ADDED, codes.FILE_ALREADY_IN_MYLIST, codes.MYLIST_ENTRY_EDITED):
                return
            elif code == codes.NO_SUCH_FILE:
                raise AniDBUnknownFile()
            elif code == codes.NO_SUCH_MYLIST_ENTRY:
                raise AniDBNotInMylist()
            elif code in (codes.LOGIN_FIRST, codes.INVALID_SESSION):
                self.auth()
            else:
                raise AniDBReplyError(code, text)

    def get_anime(self, aid=None, aname=None, amask=None, retry=False):
        args = {}
        if not aid == None:
            args['aid'] = aid
        elif not aname == None:
            args['aname'] == aname
        else:
            raise TypeError('must set either aid or aname')

        args['amask'] = amask or '00' * 7

        while 1:
            code, text, data = self.execute('ANIME', args, retry)
            if code == codes.ANIME:
                return data[0]
            elif code == codes.NO_SUCH_ANIME:
                raise AniDBUnknownAnime()
            elif code in (codes.LOGIN_FIRST, codes.INVALID_SESSION):
                self.auth()
            else:
                raise AniDBReplyError(code, text)

    def get_animedesc(self, aid, retry=False):
        args = {'aid': aid, 'part': 0}
        description = ''
        while 1:
            code, text, data = self.execute('ANIMEDESC', args, retry)
            if code == codes.ANIME_DESCRIPTION:
                curpart, maxpart, desc = data[0]
                description += desc
                if curpart == maxpart:
                    return description
                else:
                    args['part'] = int(curpart) + 1
            elif code == codes.NO_SUCH_ANIME:
                raise AniDBUnknownAnime()
            elif code == codes.NO_SUCH_DESCRIPTION:
                raise AniDBUnknownDescription()
            elif code in (codes.LOGIN_FIRST, codes.INVALID_SESSION):
                self.auth()
            else:
                raise AniDBReplyError(code, text)
